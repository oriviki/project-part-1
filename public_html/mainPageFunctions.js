/*
 * mainPageFunctions.js
 * This file holds all the functunalities needed for the main page.
 * It also holds the functions that read the .json files.
 * Written By: Victoria Sandler & Ori Shabo
 *         ID: 307462903, 300739653
 *      Login: victoriasa, orisha
 */

/*
 * This function reads the json files and shows it in the post messages
 * section of the main page.
 * input - none.
 * output- visible user names, posts and comments on main page.
 */
$(document).ready(function() {
    //read users
    $.getJSON("loggedInUsers.json", function(data) {
        var htmlCode = "";
        $.each(data, function(key, val) {
            htmlCode += "<div class=\"name\">" + val + "</div>";
        });
        $('#usersList').html(htmlCode);
    });
    //reads posts and comments
    $.getJSON("posts.json", function(data) {
        var htmlCode = "";
        var id_counter = 1;
        $.each(data.posts, function(key, val) {
            htmlCode += "<div class=\"messageWarp\" id=" + id_counter + ">";
            htmlCode += "<input type=\"image\" class=\"button\" src=\"down-arrow.png\" onclick=\"hideAndShow(this)\">";
            htmlCode += "<div class=\"\postHeader\"><b> posted by " + val.name + val.postDate + "</b></div>";
            htmlCode += "<div class=\"postBody\">" + val.post + "</div>";
            htmlCode += "<input type=\"text\" value=\"Write a comment...\" onclick=\"clearText(this)\">"
            htmlCode += "<input type=\"submit\" class=\"postCommentButton\" value=\"Post\" /> "
            htmlCode += "<div class=\"comments\">";
            $.each(val.comments, function(name, comment) {
                htmlCode += "<div class=\"usersComments\">" + comment.name + " - " + comment.comment + "</div>";
            });
            htmlCode += "</div>";
            htmlCode += "</div>";
            id_counter++;
        });
        $('#messages').html(htmlCode);

    });
});

/*
 * This functions is called when the little arrow in the post section is pressed
 * It allows the hide/show of comments.
 * input - item - the item to hide the comments of
 * output - hides/shows comment of a specific post on the main page.
 */
function hideAndShow(item) {
    var parentDivId = $(item).parent().attr("id");
    var specificid = $('#' + parentDivId + ' div.comments');
    $(specificid).slideToggle();
}

/*
 * This functions is called when the text field in the post section is pressed
 * It allows the erase of its defult text
 * input - item - the text box
 * output - erase the text
 */
function clearText(item) {
    var parentDivId = $(item).parent().attr("id");
    var specificid = $('#' + parentDivId + ' input[type=text]');
    $(specificid).val("");
}