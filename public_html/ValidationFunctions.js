/*
 * ValidationFunctions.js
 * This file holds all the validation functions for the login and registration
 * forms.
 * Written By: Victoria Sandler & Ori Shabo
 *         ID: 307462903, 300739653
 *      Login: victoriasa, orisha
 */

/*
 * This function validates the user name in the registration form.
 * input - none.
 * output- true if user name is not empty, false otherwise.
 */
function ValidateUser() {
    //holds true if user name is valid and false otherwise.
    var valid_user = true;
    if ($('#user_name_input').val() === "") {
        $('#user_name_input').css('border-color', 'red');
        $('#user_name_error_div').html("User name cannot be empty!");
        valid_user = false;
    }
    return valid_user;
} 
   
/*
 * This function validates the users email address.
 * input - none.
 * output - true is the email is valid and false otherwise.
 */
function ValidateEmail() {
    //holds true if email address is valid and false otherwise.
    var valid_email = true;
    if ($('#user_email_input').val() === "") {
        $('#user_email_input').css('border-color', 'red');
        $('#user_email_error_div').html("Email canno't be empty!");
        valid_email = false;
    }            
    var email = $('#user_email_input').val();
    var patt = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/igm;
    if (patt.test(email) === false) {
        $('#user_email_input').css('border-color', 'red');
        $('#user_email_error_div').html("Email not vaild! shoul'd be like user@com.com");
        valid_email = false;
    }
    return valid_email;
} 
/*
 * This function validates the passwors the user chose.
 * The password must be of at least 8 characters long and must contain digits
 * and characters.
 * input - none.
 * output - true is the password is valid and false otherwise.
 */           
function ValidatePassword() {
    var password = $('#password_input').val();
    //check length of password
    if ($('#password_input').val().length < 8) {
        $('#password_input').css('border-color', 'red');
        $('#user_password_error_div').html("Password must have at least 8 characters!");
        return false;
    }
    //check structure of password
    patt = /^(?=.*[A-Za-z])(?=.*[0-9])(?!.*[^A-Za-z0-9])(?!.*\s).{8,}$/;
    if (patt.test(password) === false) {
        $('#password_input').css('border-color', 'red');
        $('#user_password_error_div').html("Password must have Characters and digists!");
        return false;
    }
    return true;
}
/*
 * This function is called when the user presses the submit button on the
 * registration form.
 * input- none.
 * output - true if the form is valid, false otherwise.
 */          
function ValidateForm(){
    var valid_Name = true;
    var valid_Email = true;
    var valid_psw = true;
    valid_Name = ValidateUser();
    valid_Email = ValidateEmail();
    valid_psw = ValidatePassword();
    return (valid_Name && valid_Email && valid_psw);
}
/*
 * This function is called when the user presses the login button in the login
 * form.
 * input - none.
 * output - true if the data the user inserted is correct and false otherwise.
 */
function ValidateLogin(){
    var valid_pass = true;
    var valid_user = true;
    valid_user = ValidateEmail();
    valid_pass = ValidatePassword();
    if(valid_user && valid_pass)
        return true;
    return false;
}

/*
 * This function is called when there is a need to remove the error markers
 * from the input boxes.
 * input - textID - the name of input box
 *         divID - the name of related div area
 */
function removeError(textID, divID) {
    $(textID).removeClass('elegant-aero-error');
    $(textID).css('border-color', '#CEE2E7');
    $(divID).html("");
}

/*
 * This function clears all divs and input boxes from error messages.
 * input - none.
 * output - none.
 */
function clearForm(){
    removeError('#user_name_input', '#user_name_error_div');
    removeError('#user_email_input', '#user_email_error_div');
    removeError('#password_input', '#user_password_error_div');
}